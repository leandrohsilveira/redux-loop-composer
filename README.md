# Redux-loop composer

Easely create actions, handle reducers and it's side effects thanks to [redux-loop](https://redux-loop.js.org/)


## Quick start

`npm i -s redux-loop-composer`

### 1. Create a module
```js
import { Domain } from 'redux-loop-composer';

const loginDomain = new Domain('login');

export default loginDomain;
```

### 2. Export actions
```js
export const login = (username, password) => loginDomain.action('login').payload({ username, password });
export const loginSuccess = (user) => loginDomain.action('loginSuccess').payload({ user });
export const loginFailure = (error) => loginDomain.action('loginFailure').payload({ error });
export const logout = () => loginDomain.action('logout').noPayload();
export const tokenExpired = () => loginDomain.action('tokenExpired').noPayload();
```

### 3. Handle reducers
```js
loginDomain
    // initial state
    .reducer({
        logging: false,
        loginSuccess: false,
        user: null
    })
    .when('login')
    .evolve((payload, state) => ({ logging: true, loginSuccess: false })) // change state

    .when('loginSuccess')
    .evolve(({ user }, state) => ({ logging: false, loginSuccess: true, user })) // change state

    .when('loginFailure')
    .evolve((payload, state) => ({ logging: false, loginSuccess: false })) // change state

    .when(['logout', 'tokenExpired'])
    .overwrite((payload, state) => ({user: null})) // overwrite state

    .end(); // finish reducer composition
```

### 4. Schedule side effects
Handle side effects with [redux-loop](https://redux-loop.js.org/):
```js
loginDomain
    .sideEffects()

    .when('login')
    .run(loginService.login) // remote call
    .args(({ username, password }, state) => [username, password]) // extract arguments from current action payload
    .then(loginSuccess) // success action
    .catch(loginFailure) // failure action

    .when('loginSuccess')
    .action(showMessage) // simply dispatch another action
    .args('Login successful') // static argument passed to the new action

    .when('loginFailure')
    .action(showErrorMessage) // simply dispatch another action
    .args(({error}, state) => [error.message]) // extract arguments from current action payload

    .when('tokenExpired')
    .cmd((cmd, payload, state) => cmd.action(showMessage('Session expired, please login again to proceed'))) // manually invoke Cmds

    .end(); // finish side effects composition
```

### 5. Add an module to the appliacation
```js
import { App } from 'redux-loop-composer';
import loginDomain from './loginDomain';
import messageDomain from './messageDomain';

const app = new App('example');

app.addDomain(loginDomain);
app.addDomain(messageDomain);

export const store = app.compose();
```

### 6. Access the state from a module
Direct access:
```js
// loginDomain.js
export const getLoginState = state => loginDomain.fromStore(state).getState();

// foo.js
import { getLoginState } from './loginDomain';

store.subscribe(() => {
    const loginState = getLoginState(store.getState());
    const user = loginState.user;
});
```

Select a state value with [reselect](https://github.com/reduxjs/reselect):
```js
// loginDomain.js
export const selectUserFullName = loginDomain.selector(state => state.user).then(user => `${user.firstName} ${user.lastName}`);

// foo.js
import {selectUserFullName} from './loginDomain';

store.subscribe(() => {
    const fullName = selectUserFullName(store.getState());
    console.log(fullName);
});
```

### 7. Dispatch actions
```js
// type: example.login.login
// payload:
//  username: 'username'
//  password: 'password'
store.dispatch(login('username', 'password'));
```

## Using with React
```jsx
import React from 'react';
import connect from 'react-redux';
import { login, getLoginState } from './loginDomain';

const mapStateToProps = state => {
    const loginState = getLoginState(state);
    return {
        user: loginState.user
    }
};

const mapDispatchToProps = dispatch => ({
    login: (username, password) => dispatch(login(username, password))
});

let UserMenu = ({user, login}) => {
    return (
        <div>
            {/* display user and dispatch login action */}
        </div>
    );
};

UserMenu = connect(mapStateToProps, mapDispatchToProps)(UserMenu);

export default UserMenu;
```