import {App, Domain} from './index';

// loginService.js
const loginService = {
    login: (username, password) => new Promise((resolve, reject) => {
        console.log('Do remote request', username, password);
        if(username === 'test') {
            resolve({
                username,
                name: 'Foo'
            });
        } else {
            reject({
                message: 'Invalid username/password'
            });
        }
    })
};

// messageModule.js
const messageDomain = new Domain('message');

const showMessage = (message) => messageDomain.action('showMessage').payload({ message, type: 'info' });
const showErrorMessage = (message) => messageDomain.action('showMessage').payload({ message, type: 'error' });
const clearMessage = () => messageDomain.action('clearMessage').noPayload();

messageDomain
    .reducer({
        message: null, 
        type: null
    })

    .when('showMessage')
    .evolve(({message, type}) => ({message, type}))

    .when('clearMessage')
    .evolve(() => ({message: null, type: null}))

    .end();

messageDomain
    .sideEffects()

    .when('showMessage')
    .run(
        () => new Promise(resolve => {
            setTimeout(resolve, 5000);
        })
    )
    .then(clearMessage)

    .end();

// loginModule.js
const loginDomain = new Domain('login');

const login = (username, password) => loginDomain.action('login').payload({ username, password });
const loginSuccess = (user) => loginDomain.action('loginSuccess').payload({ user });
const loginFailure = (error) => loginDomain.action('loginFailure').payload({ error });
const logout = () => loginDomain.action('logout').noPayload();
const tokenExpired = () => loginDomain.action('tokenExpired').noPayload();

loginDomain
    .reducer({
        logging: false,
        loginSuccess: false,
        user: null
    })
    .when('login')
    .evolve(() => ({ logging: true, loginSuccess: false }))

    .when('loginSuccess')
    .evolve(({ user }) => ({ logging: false, loginSuccess: true, user }))

    .when('loginFailure')
    .evolve({ logging: false, loginSuccess: false })

    .when(['logout', 'tokenExpired'])
    .overwrite({user: null})

    .end();

loginDomain
    .sideEffects()

    .when('login')
    .run(loginService.login)
    .args(({ username, password }) => [username, password])
    .then(loginSuccess)
    .catch(loginFailure)

    .when('loginSuccess')
    .action(showMessage)
    .args('Login successful')

    .when('loginFailure')
    .action(showErrorMessage)
    .args(({error}) => [error.message])

    .when('tokenExpired')
    .cmd((cmd) => cmd.action(showMessage('Session expired, please login again to proceed')))

    .end();
    
const store = new App('example').addDomain(loginDomain).addDomain(messageDomain).compose();

describe('for loginModule test example', () => {

    describe('actions', () => {

        it('login action', () => {

            const action = login('username', 'password');

            expect(action).toStrictEqual({
                type: 'example.login.login',
                payload: {
                    username: 'username',
                    password: 'password'
                }
            });

        });

        it('loginSuccess action', () => {

            const action = loginSuccess({ username: 'username', name: 'Foo' });

            expect(action).toStrictEqual({
                type: 'example.login.loginSuccess',
                payload: {
                    user: {
                        username: 'username',
                        name: 'Foo'
                    }
                }
            });

        });

        it('loginFailure action', () => {

            const action = loginFailure({ message: 'Invalid username or password' });

            expect(action).toStrictEqual({
                type: 'example.login.loginFailure',
                payload: {
                    error: {
                        message: 'Invalid username or password'
                    }
                }
            });

        });

    });

    describe('reducers', () => {

        it('login reducer', () => {
            const action = login('test', '123456');
            const newState = loginDomain.handleReducer(undefined, action);

            expect(newState).toStrictEqual({
                logging: true,
                loginSuccess: false,
                user: null
            });
        });

        it('loginSuccess reducer', () => {
            const previousState = {logging: true, loginSuccess: false, user: null};
            const action = loginSuccess({username: 'test', name: 'Foo'});
            const newState = loginDomain.handleReducer(previousState, action);

            expect(newState).toStrictEqual({
                logging: false,
                loginSuccess: true,
                user: { username: 'test', name: 'Foo' }
            });
        });

        it('loginFailure reducer', () => {
            const previousState = {logging: true, loginSuccess: false, user: null};
            const action = loginFailure({message: 'Login failed'});
            const newState = loginDomain.handleReducer(previousState, action);

            expect(newState).toStrictEqual({
                logging: false,
                loginSuccess: false,
                user: null
            });
        });

        it('logout reducer', () => {
            const previousState = {
                logging: false, 
                loginSuccess: true, 
                user: {username: 'test', name: 'Foo'}
            };
            const action = logout();
            const newState = loginDomain.handleReducer(previousState, action);

            expect(newState).toStrictEqual({
                user: null
            });

        });

        it('tokenExpired reducer', () => {
            const previousState = {
                logging: false, 
                loginSuccess: true, 
                user: {username: 'test', name: 'Foo'}
            };
            const action = tokenExpired();
            const newState = loginDomain.handleReducer(previousState, action);

            expect(newState).toStrictEqual({
                user: null
            });

        });

    });

    describe('effects', () => {

        it('login effect', () => {

            const action = login('test', '123456');
            const state = {logging: true, loginSuccess: false, user: null};
            const effect = loginDomain.handleSideEffects(state, action);

            expect(effect.type).toEqual('RUN');
            expect(effect.args).toEqual(['test', '123456']);
            expect(effect.successActionCreator).toEqual(loginSuccess);
            expect(effect.failActionCreator).toEqual(loginFailure);
        });

        it('loginSuccess effect', () => {

            const action = loginSuccess({username: 'test', name: 'Foo'});
            const state = {logging: false, loginSuccess: true, user: {username: 'test', name: 'Foo'}};
            const effect = loginDomain.handleSideEffects(state, action);

            expect(effect.type).toEqual('ACTION');
            expect(effect.actionToDispatch).toEqual(showMessage('Login successful'));
            
        });

        it('loginFailure effect', () => {

            const action = loginFailure({message: 'Login failed'});
            const state = {logging: false, loginSuccess: false, user: null};
            const effect = loginDomain.handleSideEffects(state, action);

            expect(effect.type).toEqual('ACTION');
            expect(effect.actionToDispatch).toEqual(showErrorMessage('Login failed'));
            
        });

        it('tokenExpired effect', () => {

            const action = tokenExpired();
            const state = {
                logging: false, 
                loginSuccess: true, 
                user: {username: 'test', name: 'Foo'}
            };
            const effect = loginDomain.handleSideEffects(state, action);

            expect(effect.type).toEqual('ACTION');
            expect(effect.actionToDispatch).toEqual(showMessage('Session expired, please login again to proceed'));
            
        });

    });

});