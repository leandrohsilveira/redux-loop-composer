import { createSelector } from 'reselect';
import { Cmd, loop, combineReducers, install } from 'redux-loop';
import { createStore } from 'redux';

const DEFAULT_ACTION = { type: '___none' };

export const Domain = (name) => {

    let mapped = {};
    let effects = {};

    let nestedDomain = [];
    let initialState = {};
    let _name = name;

    const reducerOrElseMap = (payload, state) => state;
    const sideEffectsOrElseMap = () => Cmd.none;

    const thiz = {

        action,
        reducer,
        handleReducer,
        sideEffects,
        handleSideEffects,
        handleCycle,

        fromStore,
        selector,

        copyTo,
        addDomain,

        __register,
        __constructor,
    };

    return thiz;

    function copyTo(_domain) {
        _domain.__constructor(mapped, effects, nestedDomain, initialState);
    }

    function action(type) {
        const _payload = (payload) => {
            return {
                type: `${_name}.${type}`,
                payload
            };
        };
        const noPayload = () => {
            return _payload();
        }
        return {
            payload: _payload,
            noPayload
        };
    }

    function fromStore(state) {
        const getState = () => {
            return state[_name];
        };
        return {
            getState
        };
    }

    function selector(...selectors) {
        const then = (operator) => {
            const _selector = createSelector.apply(createSelector, [...selectors, operator]);
            return (state) => _selector(fromStore(state).getState());
        };
        return {
            then
        };
    }

    function reducer(_initialState = {})  {
        initialState = _initialState;

        const when = (type) => {
            
            const evolve = (storeModifier) => {
                if (Array.isArray(type)) {
                    type.forEach(_type => when(_type).evolve(storeModifier));
                } else if(typeof storeModifier === 'function') {
                    mapped[type] = (payload, state) => {
                        const changes = storeModifier(payload, state);
                        if(state === changes) {
                            return state;
                        } else {
                            return {...state, ...changes};
                        }
                    };
                } else {
                    when(type).evolve(() => storeModifier);
                }
                return {
                    when,
                    end: () => {}
                };
            };

            const overwrite = (storeModifier) => {
                if (Array.isArray(type)) {
                    type.forEach(_type => when(_type).overwrite(storeModifier));
                } else if(typeof storeModifier === 'function') {
                    mapped[type] = storeModifier;
                } else {
                    when(type).overwrite(() => storeModifier);
                }
                return {
                    when,
                    end: () => {}
                };
            }

            return {
                evolve,
                overwrite
            };
        };

        return {
            when,
            end: () => {}
        };

    }

    function sideEffects() {
        const when = (type) => {

            const _parseArgs = (argumentsExtractor) => {
                if(typeof argumentsExtractor === 'function') {
                    return argumentsExtractor;
                } else if (Array.isArray(argumentsExtractor)) {
                    return () => argumentsExtractor;
                } else {
                    return () => [argumentsExtractor];
                }
            };

            const run = (service) => {

                let _argumentsExtractor = () => [];
                const options = {};

                const _push = (_type = type) => {
                    if(Array.isArray(_type)) {
                        return _type.forEach(__type => _push(__type));
                    }
                    effects[_type] = (payload, state) => {
                        return Cmd.run(
                            service, 
                            {
                                ...options, 
                                args: _argumentsExtractor(payload, state)
                            }
                        );
                    };
                };

                const _when = (_type) => {
                    _push();
                    return when(_type);
                };

                const _catch = (action) => {
                    options.failActionCreator = action;
                    return {
                        end: _push,
                        when: _when
                    };
                };

                const then = (action) => {
                    options.successActionCreator = action;
                    return {
                        end: _push,
                        when: _when,
                        'catch': _catch
                    }
                }

                const args = (argumentsExtractor) => {
                    _argumentsExtractor = _parseArgs(argumentsExtractor);
                    return {
                        end: _push,
                        when: _when,
                        then
                    };
                };
                
                return {
                    args,
                    end: _push,
                    when: _when,
                    then
                };

            };

            const action = (actionCreator) => {

                let _argumentsExtractor = () => [];

                const _push = (_type = type) => {
                    if(Array.isArray(_type)) {
                        return _type.forEach(__type => _push(__type));
                    }
                    effects[_type] = (payload, state) => {
                        return Cmd.action(actionCreator.apply(actionCreator, _argumentsExtractor(payload, state)));
                    };
                };

                const _when = (_type) => {
                    _push();
                    return when(_type);
                }

                const args = (argumentsExtractor) => {
                    _argumentsExtractor = _parseArgs(argumentsExtractor);
                    return {
                        when: _when,
                        end: _push
                    };
                };

                const noArgs = () => {
                    return {
                        when: _when,
                        end: _push
                    };
                };

                return {
                    args,
                    noArgs
                };

            };

            const cmd = (handler) => {
                const _push = (_type = type) => {
                    if(Array.isArray(_type)) {
                        return _type.forEach(__type => _push(__type));
                    }
                    effects[_type] = (payload, state) => handler(Cmd, payload, state);
                };

                const _when = (_type) => {
                    _push();
                    return when(_type);
                }

                return {
                    when: _when,
                    end: _push
                };

            };

            return {
                run,
                action,
                cmd
            };

        };

        return {
            when
        };
    }

    function __constructor(_mapped, _effects, _nestedDomains, _initialState) {
        mapped = {..._mapped};
        effects = {..._effects};
        nestedDomain = [..._nestedDomains];
        initialState = {..._initialState};
    }

    function __register(parent) {
        if(parent) {
            _name = `${parent}.${name}`;
        }
        
        const _domains = {};

        nestedDomain.forEach(nestedDomain => {
            const flattenNestedDomains = nestedDomain(_name);
            Object.keys(flattenNestedDomains).forEach(domainName => {
                const _domain = flattenNestedDomains[domainName];
                if(_domains[_domain.name]) {
                    throw `A domain named "${_domain.name}" already exists!`;
                }
                _domains[_domain.name] = _domain;
            });
        });

        if(_domains[_name]) {
            throw `A domain named "${_name}" already exists!`;
        }

        _domains[_name] = {
            name: _name,
            reducer: handleCycle
        };

        return _domains;
    }

    function handleCycle(state = initialState, action = DEFAULT_ACTION) {
        const newState = handleReducer(state, action);
        return loop(
            newState,
            handleSideEffects(newState, action)
        );
    }

    function handleReducer(state = initialState, action = DEFAULT_ACTION) {
        let map = mapped[parseType(action.type)] || reducerOrElseMap;
        return map(action.payload, state);
    }

    function handleSideEffects(state = initialState, action = DEFAULT_ACTION) {
        return (effects[parseType(action.type)] || sideEffectsOrElseMap)(action.payload, state);
    }

    function addDomain(_domain) {
        nestedDomain.push((name) => _domain.__register(name));
    }

    function parseType(type) {
        return type.replace(`${_name}.`, '');
    }

}

export const App = (name) => {

    const appDomain = new Domain(name);
    
    const thiz = {
        addDomain,
        compose
    };

    return thiz;

    function compose(middlewareApplier) {
        const appDomains = appDomain.__register();
        const reducers = {};
        Object.keys(appDomains).forEach(domainName => {
            const _domain = appDomains[domainName];
            reducers[_domain.name] = _domain.reducer;
        });
        const combinedReducers = combineReducers(reducers);
        if(typeof middlewareApplier === 'function') {
            return createStore(combinedReducers, middlewareApplier(install()));
        } else {
            return createStore(combinedReducers, install());
        }
    }

    function addDomain(_domain) {
        appDomain.addDomain(_domain);
        return thiz;
    }
}

const composer = {
    app: App,
    create: Domain,
};

export default composer;